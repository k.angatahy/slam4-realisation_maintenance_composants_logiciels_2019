package abstraites;

public class Compte
{
    // d�finition du num�ro d'un compte
    private String numero;
    // d�finition du nom du titulaire du compte
    private String nomTitulaire;
    // d�finition du solde du compte
    private double solde;
     
    // on ne peut qu'obtenir le num�ro (en lecture) 
    /**
	 * @return the solde
	 */
	public double getSolde() {
		return solde;
	}
	
    // on ne peut que lire le nom du titulaire du compte
	/**
	 * @param solde the solde to set
	 */
	public void setSolde(double solde) {
		this.solde = solde;
	}

	// on peut obtenir et modifier le solde du compte
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @return the nomTitulaire
	 */
	public String getNomTitulaire() {
		return nomTitulaire;
	}

    
// constructeur par d�faut
    public Compte()
    {
        numero = "0000";
        nomTitulaire = "un titulaire inconnu";
        solde = 0.0;
    }

    // constructeur avec initialisation de tous les attributs de Compte en utilisant les valeurs pass�es en param�tres
    public Compte(String numero, String nomTitulaire, double solde)
    {
        this.numero = numero;
        this.nomTitulaire = nomTitulaire;
        this.solde = solde;
    }
    
}


