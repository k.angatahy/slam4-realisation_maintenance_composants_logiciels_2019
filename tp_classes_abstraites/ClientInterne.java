package abstraites;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClientInterne extends Salarie implements Client
{
	
	protected double soldeCompte;
	protected List<Compte> lesComptes;
	

	public ClientInterne(int salaire, String nom, String prenom, double soldeCompte) {
		super(salaire, nom, prenom);
		lesComptes = new ArrayList<Compte>();
		this.soldeCompte = soldeCompte;
	}
	



	@Override
	public void creerCompte() {
		// TODO Auto-generated method stub
		Scanner saisie = new Scanner(System.in);
        
	       System.out.println("\n  ===> CREATION DE COMPTE - pour client interne");
	       System.out.print("entrer le nom du titulaire : ");
	       String nom = saisie.next();
	       System.out.print("\nentrer le num�ro du compte : ");
	       String num = saisie.next();
	       System.out.print("entrer le solde : ");
	       double solde = saisie.nextDouble();

	       lesComptes.add(new Compte(num, nom, solde));
		
	}




	public double getSoldeCompte() {
		return soldeCompte;
	}

	@Override
	public void verserSalaire() {
		// TODO Auto-generated method stub
		
		soldeCompte += getSalaire();
		System.out.println("un salaire de " + getSalaire() + "à été versé à " + getNom() +" "+ getPrenom() + ".");
		
		
	}




	@Override
	public String toString() {
		return "ClientInterne [soldeCompte=" + soldeCompte + ", lesComptes=" + lesComptes + ", salaire=" + salaire
				+ ", nom=" + nom + "]";
	}
	
	
	
	

}
