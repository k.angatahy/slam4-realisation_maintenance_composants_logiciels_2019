package abstraites;

public class Employe extends Salarie{

	protected String nomBanque;
	

	public Employe(int salaire, String nom, String prenom, String nomBanque) {
		super(salaire, nom, prenom);
		this.nomBanque = nomBanque;
	}
	
	



	@Override
	public String toString() {
		return "Employe [nomBanque=" + nomBanque + ", salaire=" + salaire + ", nom=" + nom + "]";
	}





	@Override
	public void verserSalaire() {
		// TODO Auto-generated method stub
		
		System.out.println("un chèque de "+ getSalaire() + " à été envoyé à " + getNom() +" "+ getPrenom() + ".");
		
	}
	
	
	
}
