package abstraites;

public abstract class Salarie {
	
	protected int salaire;
	protected String nom;
	private String prenom;
	
	
	public int getSalaire() {
		return salaire;
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public Salarie(int salaire, String nom, String prenom) {
		super();
		this.salaire = salaire;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public abstract void verserSalaire();

	@Override
	public String toString() {
		return "Salarie [salaire=" + salaire + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
	
	
}
