package abstraites;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author geraldine
 *
 */
public class ClientExterne extends Salarie implements Client
{
     // liste des comptes du client
    private List<Compte> lesComptes;
    // nom du client externe
    private String nom;
    //pr�nom du client externe
    private String prenom;

     // la liste des comptes ne peut �tre modifi�e directement
    public List<Compte> getLesComptes()
    {
         return lesComptes; 
    }

    // acc�s en lecture au nom
    public String getNom()
   {
       return nom;  
   }

    // acc�s en lecture au pr�nom
   public String getPrenom()
   {
       return prenom; 
   }
 
    
    
   
   

public ClientExterne() {
		super(0, "", "");
		lesComptes =  new ArrayList<Compte>();
		nom = "";
		prenom = "";
	}

public ClientExterne(int salaire, String nom, String prenom, String nom2, String prenom2) {
		super(salaire, nom, prenom);
		lesComptes = new ArrayList<Compte>();
		nom = nom2;
		prenom = prenom2;
	}

// cr�ation d'un compte pour un ClientExterne
   public void creerCompte()
   {
	   Scanner saisie = new Scanner(System.in);
        
       System.out.println("\n  ===> CREATION DE COMPTE - pour client externe");
       System.out.print("entrer le nom du titulaire : ");
       String nom = saisie.next();
       System.out.print("\nentrer le num�ro du compte : ");
       String num = saisie.next();
       System.out.print("entrer le solde : ");
       double solde = saisie.nextDouble();

       lesComptes.add(new Compte(num, nom, solde));
   }

    // r�cup�re la cha�ne des caract�ristiques d'un client externe
   @Override
    public String toString()
    {
        String chaine = null;
        chaine = "\nComptes du client " + nom + " " + prenom + " : ";
        for (Compte unCompte : lesComptes)
            chaine += "\ncompte n� " + unCompte.getNumero() + "\t nom du titulaire : " + unCompte.getNomTitulaire() + " \t solde : " + unCompte.getSolde() + " euros";
        return  "\n" + chaine;
    }

	@Override
	public void verserSalaire() {
		// TODO Auto-generated method stub
		
	}   
}


