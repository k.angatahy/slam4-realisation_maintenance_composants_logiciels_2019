package abstraites;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		/*
	Salarie s = new Salarie();
	s.toString();
	s.verserSalaire();
	*/ 
		//exercice precedent
//	Employe e = new Employe(1200, "stockNom", "stockPrenom", "BNP Paribas");
//	System.out.println(e.toString());
//	e.verserSalaire();
//	
//	ClientInterne ci = new ClientInterne(1500, "bigard", "jean-marie", 500000);
//	System.out.println(ci.toString());
//	ci.verserSalaire();
		/**
		 * Salari� instance de employ�
		 */
		Salarie s1 = new Employe(1500, "Lau", "Pete", "Caisse d'Epargne");
		System.out.println(s1.toString()); // affichage des attributs du Salarie
		System.out.println(s1.getNom());
		System.out.println(s1.getPrenom());
		System.out.println(s1.getSalaire());
		
		
		/**
		 * Salari� instance de Client interne
		 */
		Salarie s2 = new ClientInterne(1900, "Xi", "Dave", 15000);
		System.out.println(s2.toString()); // affichage des attributs du salarie
		
		/**
		 * Employ� instance de Employ�
		 */
		Employe e1 = new Employe(1000, "deez", "nuts", "Boursorama");
		System.out.println(e1.toString()); // affichage des attributs de Employ�
		
		/**
		 * client interne instance de client interne	
		 */
		ClientInterne ci1 = new ClientInterne(1500, "west", "kanye", 99999999);
		ci1.creerCompte(); // Cr�ation du compte du client interne 
		System.out.println(ci1.toString()); // affichage des attributs de client interne
		System.out.println(ci1.getSoldeCompte());
		
		/**
		 * un client instance de client interne 
		 */
		Client c1 = new ClientInterne(1500, "curry", "ayesha", 14000);
		c1.creerCompte(); // creation du compte du client - client interne
		System.out.println(c1.toString()); // affichage des attributs du client
		
		/*
		 * client instance de client externe
		 */
		Client c2 = new ClientExterne(6522, "james", "lebron", "james", "lebron");
		c2.creerCompte(); // creation du compte du client - client externe
		System.out.println(c2.toString());
		
		/*
		 * client externe instance de client externe
		 */
		ClientExterne ce1 = new ClientExterne(16500, "gates", "bill", "gates", "bill");
		ce1.creerCompte(); // creation du compte de client externe instance de client externe
		System.out.println(ce1.toString()); // affichage des attributs du client externe 
	
	}

}
