package les_classes;

public class Cercle_2 extends Point {

    private double rayon;


    //public String centre = "  " + x + ", " + y + " ";
    static public String unite = "cm";



    public Cercle_2() {
        super();
        rayon = 0;

    }

    public Cercle_2(double x, double y, double rayon) {

        super(x, y);
        this.rayon = rayon;

    }

    public void changerRayon(double rayon){

        this.rayon = rayon;

    }


    public void deplaceCentre(double x, double y){

        super.deplace(x, y);

    }

    public static void setUnite(String unite) {
        Cercle_1.unite = unite;
    }

    @Override
    public String toString() {
        return "Cercle_2{" +
                "rayon=" + rayon +
                '}'
                + super.toString();
    }
}
