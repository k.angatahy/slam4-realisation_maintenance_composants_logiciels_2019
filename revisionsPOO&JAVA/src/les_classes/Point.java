package les_classes;

public class Point {
    protected double x, y;
    static protected String unite = "cm";

    public Point(){

        x=y=0;


    }

    public Point(double x, double y){

        this.x = x;
        this.y = y;

    }

    public void deplace(double dx, double dy){

        x += dx;
        y += dy;

    }

    public static void setUnite(String unite) {
        Point.unite = unite;
    }



    public String toString(){

        return "Point de coordonnées : " + x + unite + ", " + y + unite;

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public static String getUnite() {
        return unite;
    }
}

