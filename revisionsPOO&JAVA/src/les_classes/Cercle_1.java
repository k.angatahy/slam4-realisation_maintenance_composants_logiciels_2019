package les_classes;



public class Cercle_1 {
    private double rayon;
   private Point centre;

    //public String centre = "  " + x + ", " + y + " ";
    static public String unite = "cm";



    public Cercle_1() {
       centre = new Point();
        rayon = 0;

    }

    public Cercle_1(double rayon, Point centre) {

        this.centre = centre;
        this.rayon = rayon;

    }

    public void changerRayon(double rayon){

        this.rayon = rayon;

    }

    public Point getCentre() {
        return centre;
    }

    public void deplaceCentre(double x, double y){

       centre.deplace(x, y);

    }

    public static void setUnite(String unite) {
        Cercle_1.unite = unite;
    }

    @Override
    public String toString() {
        return "Cercle_1{" +
                "rayon=" + rayon +
                ", centre=" + centre +
                '}';
    }
}
